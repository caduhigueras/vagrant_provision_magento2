#!/usr/bin/env bash

#########################################################################
############ Initital setup - Define here all your variables ############
#########################################################################

staticip=192.168.68.80 #as defined on vagrant file:
xdebugidename=PHPSTORM #if you are using xdebug, change this variable at m2files/xdebug.ini before running vagrant up

#items necessary for magento install
dbname=magento2 #database name to install magento 2
dbpass=bnm196922 #user will be root
magentourl=http://m2one.test #url will you put also on hosts file and use to access the website locally
magentourlnohttp=m2one.test #necessary to setup etc/hosts on nginx
magentoadminname=admin #first name of magento admin - please use just one word
magentoadminlastname=admin #last name of magento admin - please use just one word
magentoadminpassword=bnm196922 #pwd to login on magento 2
backendfrontname=admin #path to magento's backend example: http://yourmagento2.test/admin
adminemail=admin@admin.com #magento admin email
adminuser=admin #magento admin user - used to login on the backend
language=en_US #languages @Magento\Framework\Locale\Config.php: af_ZA, ar_DZ, ar_EG, ar_KW, ar_MA, ar_SA, az_Latn_AZ, be_BY, bg_BG, bn_BD, bs_Latn_BA, ca_ES, cs_CZ, cy_GB, da_DK, de_AT, de_CH, de_DE, el_GR, en_AU, en_CA, en_GB, en_NZ, en_US, es_AR, es_CO, es_PA, gl_ES, es_CR, es_ES, es_MX, eu_ES, es_PE, et_EE, fa_IR, fi_FI, fil_PH, fr_CA, fr_FR, gu_IN, he_IL, hi_IN, hr_HR, hu_HU, id_ID, is_IS, it_CH, it_IT, ja_JP, ka_GE, km_KH, ko_KR, lo_LA, lt_LT, lv_LV, mk_MK, mn_Cyrl_MN, ms_Latn_MY, nl_NL, nb_NO, nn_NO, pl_PL, pt_BR, pt_PT, ro_RO, ru_RU, sk_SK, sl_SI, sq_AL, sr_Cyrl_RS, sv_SE, sw_KE, th_TH, tr_TR, uk_UA, vi_VN, zh_Hans_CN, zh_Hant_HK, zh_Hant_TW, es_CL, lo_LA, es_VE, en_IE
currency=EUR #currency - USD, EUR, BRL etc
timezone=Europe/Madrid #timezones: Europe/Lisbon, Europe/London, Europe/Madrid, Europe/Paris, America/Chicago, America/Sao_Paulo

################################################
############ End of Initital setup  ############
################################################

sudo su

# update
echo "################################"
echo "##### PREPARING FOR PHP7.2 #####"
echo "################################"
add-apt-repository -y ppa:ondrej/php


# update
echo ""
echo ""
echo "########################"
echo "##### UPDATING APT #####"
echo "########################"
sudo apt-get -y update

# Install Nginx
echo ""
echo ""
echo "#############################"
echo "##### INSTALLING Nginx #####"
echo "#############################"
sudo apt-get -y install nginx

# Install Mysql
echo ""
echo ""
echo "#############################"
echo "##### INSTALLING MySQL #####"
echo "#############################"
# Setting MySQL root user password root/root
debconf-set-selections <<< "mysql-server mysql-server/root_password password $dbpass"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $dbpass"

# Installing packages
apt-get install -y mysql-server mysql-workbench

# Install PHP
echo ""
echo ""
echo "###########################################"
echo "##### INSTALLING PHP and Dependencies #####"
echo "###########################################"
sudo sudo apt-get -y install php7.2-fpm php7.2-gd php7.2-mysql php7.2-curl php7.2-soap php7.2-xml php-xdebug
sudo apt-get -y install php-pear
sudo apt-get -y install php-oauth php7.2-intl php7.2-mbstring php7.2-zip openssl php7.2-bcmath
sudo apt-get -y install zip unzip

# variables for mysql
echo ""
echo ""
echo "###########################################"
echo "##### CREATING DATABASE #####"
echo "###########################################"

mysql -u root -p$dbpass -e "CREATE DATABASE IF NOT EXISTS $dbname;"
# mysql $dbName -u root < /vagrant/data/db/backup_migrate/backup.mysql

## Creating folder
#echo ""
#echo ""
#echo "#########################################"
#echo "##### CREATING MAGENTO FOLDER #####"
#echo "#########################################"
#sudo mkdir /var/www/html/magento2

## nginx conf file
echo ""
echo ""
echo "#########################################"
echo "##### NGINX CONF FILE #####"
echo "#########################################"

sudo touch /etc/nginx/sites-available/m2one.test

#sudo bash -c 'echo "upstream fastcgi_backend {
#     server  unix:/run/php/php7.2-fpm.sock;
# }
#
# server {
#     listen 80 default_server;
#     server_name m2one.test;
#     set \$MAGE_ROOT /var/www/html/magento2;
#     include /var/www/html/magento2/nginx.conf.sample;
# }" > /etc/nginx/sites-available/m2one.test'

sudo cp /vagrant/m2files/m2one.test  /etc/nginx/sites-available/

sudo bash -c 'echo "127.0.0.1  $magentourlnohttp" >> /etc/hosts'

sudo rm /etc/nginx/sites-available/default
sudo rm /etc/nginx/sites-enabled/default

echo ""
echo ""
echo "#########################################"
echo "##### Creating Symlink #####"
echo "#########################################"

sudo ln -s /etc/nginx/sites-available/m2one.test /etc/nginx/sites-enabled/m2one.test

# Composer Installation
echo ""
echo ""
echo "###############################"
echo "##### INSTALLING COMPOSER #####"
echo "###############################"

curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/bin --filename=composer

# Changing php.ini for magento 2
echo ""
echo ""
echo "#########################################"
echo "##### CHANGING PHP.INI FOR MAGENTO 2#####"
echo "#########################################"
sudo rm /etc/php/7.2/fpm/php.ini
#git clone https://bitbucket.org/caduhigueras/vagrant_provision_magento2/src/master/php.ini
#sudo mv php.ini/php.ini /etc/php/7.2/fpm/
sudo cp /vagrant/m2files/php.ini /etc/php/7.2/fpm/
sudo rm -rf php.ini

# auth.json
echo ""
echo ""
echo "################################################"
echo "##### PREPARING COMPOSER FOR MAGENTO2 KEYS #####"
echo "################################################"
sudo mkdir ~/.composer/
sudo cp /vagrant/m2files/auth.json  ~/.composer/

# Composer creating Magento 2 project
echo ""
echo ""
echo "##############################################"
echo "##### CREATING COMPOSER PROJECT MAGENTO2 #####"
echo "##############################################"
cd /var/www/html
sudo composer create-project --repository=https://repo.magento.com/ magento/project-community-edition magento2

# PERMISSIONS Magento 2
echo ""
echo ""
echo "###########################################"
echo "##### SETTINP UP PERMISSIONS MAGENTO2 #####"
echo "###########################################"
cd /var/www/html/magento2
sudo find var generated vendor pub/static pub/media app/etc -type f -exec chmod g+w {} +
sudo find var generated vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} +
sudo chown -R :www-data . # Ubuntu
#sudo chmod u+x bin/magento

# install Magento 2 project
echo ""
echo ""
echo "###############################"
echo "##### INSTALLING MAGENTO2 #####"
echo "###############################"
bin/magento setup:install --base-url=$magentourl --db-host=localhost --db-name=$dbname --db-user=root --db-password=$dbpass --backend-frontname=$backendfrontname --admin-firstname=$magentoadminname --admin-lastname=$magentoadminlastname --admin-email=$adminemail --admin-user=$adminuser --admin-password=$magentoadminpassword --language=$language --currency=$currency --timezone=$timezone --use-rewrites=1

# set developer
echo ""
echo ""
echo "#######################################"
echo "##### MAGENTO2 SET DEVELOPER MODE #####"
echo "#######################################"
bin/magento deploy:mode:set developer

# clean cache
echo ""
echo ""
echo "#######################################"
echo "##### MAGENTO2 CLEAN CACHE #####"
echo "#######################################"
bin/magento c:c

# reindex
echo ""
echo ""
echo "#######################################"
echo "##### MAGENTO2 REINDEX #####"
echo "#######################################"
bin/magento indexer:reindex

echo ""
echo ""
echo "#######################################"
echo "##### MAGENTO2 RUN DI COMPILE #####"
echo "#######################################"
# compile
bin/magento setup:di:compile

echo ""
echo ""
echo "#########################################"
echo "##### MAGENTO2 FORCE DEPLOY CONTENT #####"
echo "#########################################"
# compile
bin/magento setup:static-content:deploy -f

echo ""
echo ""
echo "###################################"
echo "##### Adding configure xdebug #####"
echo "###################################"
sudo rm /etc/php/7.2/mods-available/xdebug.ini
sudo cp /vagrant/m2files/xdebug.ini /etc/php/7.2/mods-available

echo ""
echo ""
echo "#########################################"
echo "##### restarting NGINX / PHP #####"
echo "#########################################"

sudo service nginx reload
sudo service nginx restart
sudo service php7.2-fpm reload
sudo service php7.2-fpm restart

echo ""
echo ""
echo "############################################"
echo "##### You are all set! Don't forget to #####"
echo "############################################"
echo "#####################################################################"
echo "1. set your hosts file to map the ip with the site name chosen. (on Windows: C:\Windows\System32\drivers\etc\hosts)"
echo "Just add the following at your hosts file: $staticip  $magentourl"
echo "2. download xdebug helper with your favorite browser and set it to: $xdebugidename"
echo "3. configure the xdebug on your chosend IDE"
echo "4. You can access your site at: $magentourlnohttp"
echo "5. You can access the backend at: $backendfrontname"
echo "6. To access the ssh run: vagrant ssh-config to copy the private_key path and the port"
echo "and then run ssh vagrant@localhost -p yourport -i your_private_key_path"
echo "7. If you are using Windows, go to C:\Users\youruser\.ssh and rename the file known_hosts to known_hosts_bkp"
echo "8. Have fun!"
echo "######################################################################"